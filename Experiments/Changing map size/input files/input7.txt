agents:
-   start: [9, 0]
    goal: [9, 0]
    name: agent0
-   start: [8, 0]
    goal: [8, 0]
    name: agent1
-   start: [7, 0]
    goal: [7, 0]
    name: agent2
-   start: [6, 0]
    goal: [6, 0]
    name: agent3
-   start: [5, 0]
    goal: [5, 0]
    name: agent4
-   start: [4, 0]
    goal: [4, 0]
    name: agent5
map:
    dimensions: [70, 70]
    obstacles:
        - !!python/tuple [8, 8]
        - !!python/tuple [7, 8]

        - !!python/tuple [5, 5]
        - !!python/tuple [4, 5]

        - !!python/tuple [16, 16]
        - !!python/tuple [15, 16]
        - !!python/tuple [14, 16]

        - !!python/tuple [10, 10]
        - !!python/tuple [9, 10]
        - !!python/tuple [8, 10]

        - !!python/tuple [26, 26]
        - !!python/tuple [25, 26]
        - !!python/tuple [24, 26]
        - !!python/tuple [23, 26]

        - !!python/tuple [15, 20]
        - !!python/tuple [14, 20]
        - !!python/tuple [13, 20]
        - !!python/tuple [12, 20]

        - !!python/tuple [36, 36]
        - !!python/tuple [35, 36]
        - !!python/tuple [34, 36]
        - !!python/tuple [33, 36]

        - !!python/tuple [25, 30]
        - !!python/tuple [24, 30]
        - !!python/tuple [23, 30]
        - !!python/tuple [22, 30]

        - !!python/tuple [46, 46]
        - !!python/tuple [45, 46]
        - !!python/tuple [44, 46]
        - !!python/tuple [43, 46]

        - !!python/tuple [35, 40]
        - !!python/tuple [34, 40]
        - !!python/tuple [33, 40]
        - !!python/tuple [32, 40]

        - !!python/tuple [56, 56]
        - !!python/tuple [55, 56]
        - !!python/tuple [54, 56]
        - !!python/tuple [53, 56]

        - !!python/tuple [45, 50]
        - !!python/tuple [44, 50]
        - !!python/tuple [43, 50]
        - !!python/tuple [42, 50]

        - !!python/tuple [67, 66]
        - !!python/tuple [66, 66]
        - !!python/tuple [65, 66]
        - !!python/tuple [64, 66]
        - !!python/tuple [63, 66]

        - !!python/tuple [56, 60]
        - !!python/tuple [55, 60]
        - !!python/tuple [54, 60]
        - !!python/tuple [53, 60]
        - !!python/tuple [52, 60]

    # - !!python/tuple [2, 1]
    
    pickupStation:
        - !!python/tuple [69, 69]
      
        - !!python/tuple [0, 69]
      
        - !!python/tuple [35, 69]
      
    deliveryStation:
        - !!python/tuple [0, 0]
order:
    orders_:
        -   id_code: o0
            timestep: 1
            requested_quantities: [1]
            pickupStation:
                - !!python/tuple [69, 69]
        
        -   id_code: o1
            timestep: 1
            requested_quantities: [1]
            pickupStation:
                - !!python/tuple [35, 69]
        
        -   id_code: o2
            timestep: 1
            requested_quantities: [1]
            pickupStation:
                - !!python/tuple [0, 69]

        -   id_code: o3
            timestep: 10
            requested_quantities: [1]
            pickupStation:
                - !!python/tuple [0, 69]

        -   id_code: o4
            timestep: 15
            requested_quantities: [1]
            pickupStation:
                - !!python/tuple [35, 69]
        
        -   id_code: o5
            timestep: 25
            requested_quantities: [1]
            pickupStation:
                - !!python/tuple [69, 69]

        -   id_code: o6
            timestep: 40
            requested_quantities: [1]
            pickupStation:
                - !!python/tuple [0, 69]
            
        -   id_code: o7
            timestep: 40
            requested_quantities: [1]
            pickupStation:
                - !!python/tuple [35, 69]

dynamic_obstacles: {}
